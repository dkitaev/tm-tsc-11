package ru.tsc.kitaev.tm.api.controller;

import ru.tsc.kitaev.tm.model.Project;

public interface IProjectController {

    void createProjects();

    void showProjects();

    void showProject(Project project);

    void clearProjects();

    void showById();

    void showByIndex();

    void showByName();

    void removeById();

    void removeByName();

    void removeByIndex();

    void updateByIndex();

    void updateById();

}
