package ru.tsc.kitaev.tm.api.repository;

import ru.tsc.kitaev.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(int index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(int index);

    boolean existsById(String id);

    boolean existsByIndex(int index);

}
